from pathlib import Path
import subprocess
import time

from helper import logs
import config

file = 'backups'

process = None
backing_up = False


def get_current_backup_folder():
    return Path(config.backup_logs_folder, 'backups', time.strftime('%Y-%m-%d'))


def get_current_backup_path():
    folder = get_current_backup_folder()
    return Path(folder, time.strftime('%H:%M'))


def backup_server():
    logs.log(file, 'Starting server backup...')
    start_time = int(time.time())
    global backing_up
    backing_up = True

    process.send_command('stop')
    time.sleep(5)

    archive_path = get_current_backup_path().with_suffix('.tar.gz')
    backup_folder = get_current_backup_folder()
    if not backup_folder.exists():
        backup_folder.mkdir(parents=True)
    subprocess.run(['tar', '-czf', str(archive_path), 'server'])

    end_time = int(time.time())
    backup_time = end_time - start_time
    logs.log(file, f'Backed the server up in {backup_time} seconds')
    backing_up = False


def schedule_backups(server_process):
    global process
    process = server_process
    backup_times = config.backup_times
    for backup_time in backup_times:
        if len(backup_time) == 4:
            time_with_zero = f'0{backup_time}'
            index = backup_times.index(backup_time)
            backup_times[index] = time_with_zero
    s = 's' if len(backup_times) != 1 else ''
    logs.log(file, f'Scheduled {len(backup_times)} backup{s}')
    while True:
        current_time = time.strftime('%H:%M')
        if current_time in backup_times:
            archive_path = get_current_backup_path().with_suffix('.tar.gz')
            if not archive_path.exists():
                backup_server()

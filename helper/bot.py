import asyncio
import time

import discord

from helper import commands, list, logs
import config

file = 'bot'

client = discord.Client()
process = None
loop = None
started = False
console_queue = []
chat_queue = []
sent_last_message = False


@client.event
async def on_ready():
    logs.log(file, f'Logged in as {client.user}')
    global started
    started = True
    for message in console_queue:
        send_console_message(message)
    for message in chat_queue:
        send_chat_message(message)
    activity = discord.Activity(name=config.discord_activity_name, type=config.discord_activity_type)
    await client.change_presence(activity=activity)


@client.event
async def on_message(message):
    author = message.author
    if author.bot:
        return

    if client.user in message.mentions:
        await message.add_reaction('👋')

    content = message.clean_content.strip()
    if content.startswith('$'):
        logs.log(file, f'Command from {author}: {content}')
        await commands.parse_discord_command(message, process)
    elif message.channel.id == config.discord_console_channel:
        logs.log(file, f'Server command from {author}: {content}')
        process.send_command(message.clean_content)
    elif message.channel.id == config.discord_chat_channel:
        logs.log(file, f'Chat message from {author}: {content}')
        split = content.split(' ')
        words = []
        for word in split:
            colored_word = word
            if '&' in word and word.strip() != '&':
                colored_word = word.replace('&', '§')
            words.append(colored_word)
        content = ' '.join(words).replace('"', '\\"')
        color = 'c' if message.author.guild_permissions.administrator else '9'
        process.send_chat(f'§{color}{str(message.author)}:§r {content}')


async def async_send_console_message(channel, message, last_message):
    await channel.send(message)
    if last_message:
        channel = get_player_list_channel()
        await async_update_player_list('**The server is not online.**', channel)
        global sent_last_message
        sent_last_message = True


def send_console_message(message, last_message=False):
    if not started:
        console_queue.append(message)
        return
    channel_id = config.discord_console_channel
    channel = client.get_channel(channel_id)
    # noinspection PyTypeChecker
    asyncio.run_coroutine_threadsafe(async_send_console_message(channel, message, last_message), loop)


def send_chat_message(message):
    if not started:
        chat_queue.append(message)
        return
    channel_id = config.discord_chat_channel
    channel = client.get_channel(channel_id)
    # noinspection PyTypeChecker
    asyncio.run_coroutine_threadsafe(async_send_console_message(channel, message, False), loop)


def get_player_list_channel():
    channel_id = config.discord_player_channel
    return client.get_channel(channel_id)


async def async_update_player_list(formatted, channel):
    message = (await channel.history(limit=1).flatten())[0]
    await message.edit(content=formatted)


def update_player_list():
    time.sleep(1)
    players = list.get_list(process)
    formatted = list.discord_format_list(players)
    channel = get_player_list_channel()
    # noinspection PyTypeChecker
    asyncio.run_coroutine_threadsafe(async_update_player_list(formatted, channel), loop)


def start(event_loop, server_process):
    global loop, process
    loop = event_loop
    process = server_process
    loop.run_until_complete(client.start(config.discord_bot_token))

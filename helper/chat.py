from threading import Thread
import subprocess

import cv2
import pytesseract

from helper import bot, commands, list, logs
import config

file = 'chat'
previous_messages = []
process = None


def get_chat_from_image():
    image = cv2.imread('chat.png')
    text = pytesseract.image_to_string(image)
    return text


def parse_chat(chat):
    lines = chat.split('\n')
    messages = []
    global previous_messages
    previous_previous_messages = previous_messages
    previous_messages = []
    for line in lines:
        if line.startswith('<') and '>' in line:
            parts = line.split('>')
            if len(parts) >= 2:
                name = parts[0][1:].strip()
                message = parts[1].strip()
                if (name, message) not in previous_previous_messages:
                    previous_previous_messages.remove((name, message))
                    messages.append((name, message))
                previous_messages.append((name, message))
    return messages


def check_for_chat(server_process):
    global process
    process = server_process
    while True:
        subprocess.run(f'{config.adb_path} exec-out screencap -p > chat.png', shell=True)
        chat = get_chat_from_image()
        messages = parse_chat(chat)
        for message in messages:
            if message[0] in list.afk_players:
                list.toggle_afk(process, message[0])
            if message[1].startswith('$'):
                logs.log(file, f'In-game command from {message[0]}: {message[1]}')
                Thread(target=commands.parse_minecraft_command, args=(message[0], message[1], process)).start()
            else:
                logs.log(file, f'{message[0]}: {message[1]}')
                bot.send_chat_message(f'**{message[0]}:** {message[1].replace("@", "")}')

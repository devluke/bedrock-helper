from helper import backups, list, logs
import config

file = 'commands'


class Command:
    args = []

    admin_only = False
    description = 'A cool command'

    def run(self, server_process):
        logs.log(file, 'That command does not have a console implementation.', True)

    async def discord_run(self, server_process, message):
        await message.channel.send('That command does not have a Discord implementation.')

    def minecraft_run(self, server_process, gamertag):
        server_process.send_chat('§cThat command does not have a Minecraft implementation.')

    def check_and_run(self, server_process):
        self.run(server_process)

    async def discord_check_and_run(self, server_process, message):
        if self.admin_only and (not message.author.guild_permissions.administrator):
            await message.add_reaction('❌')
            logs.log(file, 'User does not have permission to use this command', True)
            return
        await self.discord_run(server_process, message)

    def minecraft_check_and_run(self, gamertag, server_process):
        if self.admin_only and (gamertag not in config.admin_gamertags):
            server_process.send_chat('§cYou do not have permission to use this command.')
            logs.log(file, 'User does not have permission to use this command', True)
        self.minecraft_run(server_process, gamertag)


class HelpCommand(Command):
    description = 'Shows this help list'

    def run(self, server_process):
        logs.log(file, 'List of commands:')
        descriptions = get_help_list()
        combined = {}
        combined.update(descriptions[0])
        combined.update(descriptions[1])
        for command in combined:
            logs.log(file, f'  {command} → {combined[command]}')

    async def discord_run(self, server_process, message):
        lines = ['**Admin commands:**']
        descriptions = get_help_list()
        for command in descriptions[0]:
            lines.append(f'  ${command} → *{descriptions[0][command]}*')
        lines += ['', '**Member commands:**']
        for command in descriptions[1]:
            lines.append(f'  ${command} → *{descriptions[1][command]}*')
        await message.channel.send('\n'.join(lines))

    def minecraft_run(self, server_process, gamertag):
        lines = ['§b§lCommands you can use:']
        descriptions = get_help_list()
        usable = {}
        usable.update(descriptions[1])
        if gamertag in config.admin_gamertags:
            usable.update(descriptions[0])
        for command in usable:
            lines.append(f'  §7${command} - §o{usable[command]}')
        server_process.send_chat('\n'.join(lines))


class BackupCommand(Command):
    admin_only = True
    description = 'Backs the server up'

    def run(self, server_process):
        backups.backup_server()

    async def discord_run(self, server_process, message):
        await message.add_reaction('⏳')
        self.run(server_process)
        await message.add_reaction('✅')

    def minecraft_run(self, server_process, gamertag):
        self.run(server_process)


class StopCommand(Command):
    admin_only = True
    description = 'Stops the server and wrapper'

    def run(self, server_process):
        server_process.restart_on_stop = False
        server_process.send_command('stop')

    async def discord_run(self, server_process, message):
        self.run(server_process)
        await message.add_reaction('✅')

    def minecraft_run(self, server_process, gamertag):
        self.run(server_process)


class RestartCommand(Command):
    admin_only = True
    description = 'Restarts the server'

    def run(self, server_process):
        server_process.send_command('stop')

    async def discord_run(self, server_process, message):
        self.run(server_process)
        await message.add_reaction('✅')

    def minecraft_run(self, server_process, gamertag):
        self.run(server_process)


class ListCommand(Command):
    description = 'Lists all online players'

    def run(self, server_process):
        server_process.send_command('list')

    async def discord_run(self, server_process, message):
        player_list = list.get_list(server_process)
        formatted_list = list.discord_format_list(player_list)
        await message.channel.send(formatted_list)

    def minecraft_run(self, server_process, gamertag):
        output = server_process.send_command('list', 2, False)
        for line in output:
            server_process.send_chat(line)


class SeedCommand(Command):
    description = 'Shows the world seed'

    def run(self, server_process):
        logs.log(file, str(config.world_seed))

    async def discord_run(self, server_process, message):
        seed = str(config.world_seed)
        await message.channel.send(f'**World seed:** {seed}')

    def minecraft_run(self, server_process, gamertag):
        seed = str(config.world_seed)
        server_process.send_chat(seed)


class GameruleCommand(Command):
    description = 'Lists all gamerules and their set values'

    def run(self, server_process):
        server_process.send_command('gamerule')

    async def discord_run(self, server_process, message):
        gamerule_output = server_process.send_command('gamerule', 1, False)[0]
        gamerule_lines = gamerule_output.split(', ')
        output_lines = ['**List of gamerules:**']
        for line in gamerule_lines:
            parts = line.split(' = ')
            value = parts[1]
            if value == 'true':
                value = '✅'
            elif value == 'false':
                value = '❌'
            else:
                value = f'`{value}`'
            output_lines.append(f'  {parts[0]} → {value}')
        await message.channel.send('\n'.join(output_lines))

    def minecraft_run(self, server_process, gamertag):
        gamerule_output = server_process.send_command('gamerule', 1, False)[0]
        server_process.send_chat(gamerule_output)


def clear_chat(server_process, admin):
    for _ in range(100):
        server_process.send_chat('')
    server_process.send_chat(f'§7The chat has been cleared by §b{admin}§7.')


class ClearCommand(Command):
    admin_only = True
    description = 'Clears the in-game chat'

    def run(self, server_process):
        clear_chat(server_process, 'console')

    async def discord_run(self, server_process, message):
        clear_chat(server_process, str(message.author))
        await message.add_reaction('✅')

    def minecraft_run(self, server_process, gamertag):
        clear_chat(server_process, gamertag)


class AfkCommand(Command):
    description = 'Toggles your AFK status'

    def minecraft_run(self, server_process, gamertag):
        list.toggle_afk(server_process, gamertag)


commands = {
    'help': HelpCommand(),
    'backup': BackupCommand(),
    'stop': StopCommand(),
    'restart': RestartCommand(),
    'list': ListCommand(),
    'seed': SeedCommand(),
    'gamerule': GameruleCommand(),
    'clear': ClearCommand(),
    'afk': AfkCommand(),
}


def get_help_list():
    admin = {}
    member = {}
    for command in commands:
        command_object = commands[command]
        if command_object.admin_only:
            admin[command] = command_object.description
        else:
            member[command] = command_object.description
    return admin, member


def get_command(message):
    words = message.split(' ')
    command_name = words[0][1:].strip()
    if command_name not in commands:
        return None
    command = commands[command_name]
    command.args = words[1:]
    return command


def parse_command(message, server_process):
    command = get_command(message)
    if command is None:
        logs.log(file, 'That command does not exist. Type `$help` for a list of commands.', True)
    else:
        command.check_and_run(server_process)


async def parse_discord_command(message, server_process):
    content = message.clean_content
    command = get_command(content)
    if command is not None:
        await command.discord_check_and_run(server_process, message)


def parse_minecraft_command(gamertag, message, server_process):
    command = get_command(message)
    if command is not None:
        command.minecraft_check_and_run(gamertag, server_process)

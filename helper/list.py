from helper import bot
import config

previous_players = []
afk_players = []


def get_list(server_process):
    first_output = server_process.send_command('list', 1, False)[0]
    if 'There are 0/10 players online' in first_output:
        return []
    output = server_process.send_command('list', 2, False)[1]
    if output.startswith('['):
        split = output.split('] ')
        if len(split) >= 2:
            output = '] '.join(split[1:])
        else:
            output = ''
    players = output.split(', ')
    if '' in players:
        players.remove('')
    global previous_players
    for player in players:
        if player not in previous_players:
            bot.send_chat_message(f'👋 **{player} has joined the server.**')
    for player in previous_players:
        if player not in players:
            if player in afk_players:
                afk_players.remove(player)
            bot.send_chat_message(f'👋 **{player} has left the server.**')
    previous_players = players
    return players


def discord_format_list(players):
    lines = []
    if not players:
        lines.append('**There is currently nobody online.**')
    else:
        are = 'are' if len(players) != 1 else 'is'
        s = 's' if len(players) != 1 else ''
        lines.append(f'**There {are} currently {len(players)} player{s} online:**')
    for player in players:
        prefix = ''
        if player == config.bot_gamertag:
            prefix = '[Bot] '
        elif player in afk_players:
            prefix = '[AFK] '
        lines.append(f'  {prefix}{player}')
    return '\n'.join(lines)


def toggle_afk(server_process, gamertag):
    if gamertag in afk_players:
        afk_players.remove(gamertag)
        server_process.send_chat(f'§b{gamertag}§7 is now AFK.')
    else:
        afk_players.append(gamertag)
        server_process.send_chat(f'§b{gamertag}§7 is no longer AFK.')
    bot.update_player_list()

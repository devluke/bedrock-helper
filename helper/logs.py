from pathlib import Path
import time

import colorama

from helper import bot
import config

file = 'logs'

logs = []


def log(file_name, message, error=False):
    color = colorama.Fore.RED if error else colorama.Fore.CYAN
    prefix = f'[{file_name.capitalize()}]'
    colored_prefix = f'{color}{prefix}{colorama.Fore.RESET}'
    full_message = f'{colored_prefix} {message}'
    print(full_message)
    uncolored_message = f'{prefix} {message}'
    logs.append(uncolored_message)
    bot.send_console_message(f'`{uncolored_message}`')


def console_log(message):
    print(message)
    logs.append(message)
    bot.send_console_message(f'`{message}`')


def start():
    colorama.init()


def stop():
    log(file, 'Saving console log...')
    log_dir = Path(config.backup_logs_folder, 'logs', time.strftime('%Y-%m-%d'))
    if not log_dir.exists():
        log_dir.mkdir(parents=True)
    path = Path(log_dir, time.strftime('%H:%M:%S') + '.txt')
    path.write_text('\n'.join(logs))
    colorama.deinit()

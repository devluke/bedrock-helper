import requests

from helper import logs

file = 'updater'

version = '1.0-private'


def check_minecraft_version(current_version):
    request = requests.get('https://www.minecraft.net/en-us/download/server/bedrock/')
    start = 'https://minecraft.azureedge.net/bin-linux/bedrock-server-'
    latest_version = (request.text.split(start))[1].split('.zip')[0].strip()
    if current_version is not None and latest_version != current_version:
        logs.log(file, f'New server version available: {current_version} -> {latest_version}')
    return latest_version

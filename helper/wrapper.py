from pathlib import Path
from threading import Thread
import asyncio
import subprocess
import time

from helper import bot, backups, chat, commands, logs, updater
import config

file = 'wrapper'

process = None
still_running = True
short_term_log = []
console_output = True


class Popen(subprocess.Popen):
    restart_on_stop = True

    def send_command(self, command, expected_output=0, output_to_console=True):
        encoded = (command + '\n').encode()
        global short_term_log, console_output
        short_term_log = []
        console_output = output_to_console
        self.stdin.write(encoded)
        self.stdin.flush()
        while len(short_term_log) < expected_output:
            continue
        console_output = True
        return short_term_log

    def send_chat(self, message):
        message = message.replace('"', '\\"')
        command = 'tellraw @a {"rawtext": [{"text": "' + message + '"}]}'
        self.send_command(command, False)


def listen_for_input():
    while True:
        command = input().strip()
        if command.startswith('$'):
            Thread(target=commands.parse_command, args=(command, process)).start()
        elif command:
            process.send_command(command)


def listen_for_output():
    while True:
        output = process.stdout.readline().strip().decode()
        if output == '' and process.poll() is not None:
            break
        if output:
            short_term_log.append(output)
            if console_output:
                logs.console_log(output)
            if ('Player connected' in output) or ('Player disconnected' in output) or ('Server started' in output):
                Thread(target=bot.update_player_list, daemon=True).start()
            split = output.split('] ')
            if len(split) >= 2:
                further_split = split[1].split(' ')
                if len(further_split) == 2 and further_split[0] == 'Version':
                    current_version = further_split[1].strip()
                    updater.check_minecraft_version(current_version)
    time.sleep(1)
    if process.restart_on_stop:
        if backups.backing_up:
            while backups.backing_up:
                continue
        else:
            logs.log(file, 'Server has stopped; restarting...')
            bot.send_console_message('🔄  **Server is restarting...**')
            bot.send_chat_message('🔄  **Server is restarting...**')
        start_server()
        Thread(target=listen_for_output, daemon=True).start()
        bot.process = process
        backups.process = process
        if config.chat_enabled:
            chat.process = process
    else:
        global still_running
        still_running = False


def start_server():
    server_path = Path('server', 'bedrock_server')
    if not server_path.exists():
        logs.log(file, 'No file named `bedrock_server` could be found in the `server` directory', True)
        exit(1)
    global process
    process = Popen('./bedrock_server', stdout=subprocess.PIPE, stdin=subprocess.PIPE, cwd='server')
    bot.send_console_message('✅  **Server has started!**')
    bot.send_chat_message('✅  **Server has started!**')


def start(key=None):
    # Check start key
    current_key = int(round(time.time()))
    valid_time_range = range(current_key - 10, current_key + 10)
    if key not in valid_time_range:
        logs.log(file, 'Invalid start key; exiting...', True)
        logs.log(file, 'If you see this message, you likely do not have permission to use this release', True)
        exit(1)
    logs.log(file, 'Start key is valid; continuing with startup...')

    # Start the server
    start_server()

    # Start listening for input and output
    Thread(target=listen_for_input, daemon=True).start()
    Thread(target=listen_for_output, daemon=True).start()

    # Schedule backups
    Thread(target=backups.schedule_backups, args=(process,), daemon=True).start()

    # Start the bot
    loop = asyncio.get_event_loop()
    Thread(target=bot.start, args=(loop, process), daemon=True).start()

    # Start monitoring the chat if enabled
    if config.chat_enabled:
        Thread(target=chat.check_for_chat, args=(process,), daemon=True).start()

    # Keep everything running
    try:
        while still_running:
            continue
    except KeyboardInterrupt:
        process.restart_on_stop = False
        logs.log(file, 'Forcing the server to stop is dangerous; use the `$stop` command in the future', True)

    # Save log and send stop messages
    logs.stop()
    logs.log(file, 'Server has stopped; goodbye!')
    bot.send_chat_message('❌  **Server has stopped!**')
    bot.send_console_message('❌  **Server has stopped!**', True)
    while not bot.sent_last_message:
        continue
